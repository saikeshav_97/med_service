import $ from 'jquery';
import "lazysizes";
import "../styles/styles.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";




//Handles Mobile Menu/header
let mobileMenu = new MobileMenu();

//handles ReavealOnScroll 
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

//handles smoothscroll
new SmoothScroll();


//Added Active link status on links
new ActiveLinks();


//WIll call the Modal
new Modal();
if(module.hot) {
    module.hot.accept();
}
//alert("Hello from MedService!");
console.log("This goes into console");
console.log("Another Line!");